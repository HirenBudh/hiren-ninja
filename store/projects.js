import axios from 'axios'

export const state = () => ({
    projects: []
})

export const getters = {
    getProjects: (state) => {
        return state.projects
    },
}

export const mutations = {
    addProjects (state, projects) {
        state.projects = projects
    },
}

export const actions = {
    async refreshProjects () {
        // const projects = await axios.get("https://hiren--devs.ew.r.appspot.com/hirensprojects");
        // commit('addProjects', projects.data)
    },
}
